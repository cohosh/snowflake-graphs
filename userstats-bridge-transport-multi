#!/usr/bin/env python3

# Parses dir_reqs and transport_ips CSV files produced by the bridge-extra-info
# program, deduplicates rows, sums them by date, fingerprint, and transport, and
# writes a CSV file in a format similar to
# https://metrics.torproject.org/userstats-bridge-transport.html, but with
# additional "fingerprint", "num_instances", and "coverage" columns, and without
# the "frac" column (the frac is assumed to always be 100). The "num_instances"
# columns is a lower bound on how many descriptors are expected to be published
# each day. The "coverage" column tells how many descriptors there were, on
# average, covering each moment in a day.
#
# The type of each input file is determined by the filename; i.e., they must
# have the extensions .dir_reqs.csv, .transport_ips.csv.
#
# Usage:
#   userstats-bridge-transport-multi dir_reqs/bridge-extra-infos-*.dir_reqs.csv transport_ips/bridge-extra-infos-*.transport_ips.csv > userstats-bridge-transport-multi.csv

import getopt
import os
import sys

import numpy as np
import pandas as pd

import common

def usage(f = sys.stdout):
    print(f"""\
Usage:
  {sys.argv[0]} [OPTION...] dir_reqs/bridge-extra-infos-*.dir_reqs.csv transport_ips/bridge-extra-infos-*.transport_ips.csv > userstats-bridge-transport-multi.csv

Options:
  -h, --help            show this help
""", file = f)

# Reference for user estimation algorithms:
# https://metrics.torproject.org/reproducible-metrics.html#bridge-users

if __name__ == "__main__":
    opts, input_paths = getopt.gnu_getopt(sys.argv[1:], "h", [
        "help",
    ])
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit(0)

    # Partition the input files by the type of data they contain, according to
    # filename.
    dir_reqs_paths = []
    transport_ips_paths = []
    for path in input_paths:
        basename = os.path.basename(path)
        if basename.endswith(".dir_reqs.csv"):
            dir_reqs_paths.append(path)
        elif basename.endswith(".transport_ips.csv"):
            transport_ips_paths.append(path)
        else:
            raise ValueError(f"don't know what to do with input path {path!r}")

    # Keep only the most recent "published" for each "end" and "transport". We
    # have already done this (per file) in the bridge-extra-info program. We do
    # it again here to remove overlaps in the seams between files. It would
    # suffice to do it only here, at the top level, but doing it also per file
    # reduces intermediate CSV size.
    dir_stats = (
        pd.concat(pd.read_csv(path, parse_dates = ["published", "begin", "end"]) for path in dir_reqs_paths)
        .sort_values("published")
        .groupby(["fingerprint", "nickname", "end"], dropna = False)
        .last()
        .reset_index()
    )
    transport_stats = (
        pd.concat(pd.read_csv(path, parse_dates = ["published", "begin", "end"]) for path in transport_ips_paths)
        .sort_values("published")
        .groupby(["fingerprint", "nickname", "transport", "end"], dropna = False)
        .last()
        .reset_index()
    )

    # Distribute over dates.
    dir_stats_bydate = {
        "date": [],
        "fingerprint": [],
        "nickname": [],
        "reqs": [],
        "coverage": [],
    }
    transport_stats_bydate = {
        "date": [],
        "fingerprint": [],
        "nickname": [],
        "transport": [],
        "ips": [],
        "coverage": [],
    }
    for row in dir_stats.itertuples():
        for (date, frac_int, frac_day) in common.segment_datetime_interval(row.begin, row.end):
            dir_stats_bydate["date"].append(date)
            dir_stats_bydate["fingerprint"].append(row.fingerprint)
            dir_stats_bydate["nickname"].append(row.nickname)
            dir_stats_bydate["reqs"].append(row.reqs * frac_int)
            dir_stats_bydate["coverage"].append(frac_day)
    for row in transport_stats.itertuples():
        for (date, frac_int, frac_day) in common.segment_datetime_interval(row.begin, row.end):
            transport_stats_bydate["date"].append(date)
            transport_stats_bydate["fingerprint"].append(row.fingerprint)
            transport_stats_bydate["nickname"].append(row.nickname)
            transport_stats_bydate["transport"].append(row.transport)
            transport_stats_bydate["ips"].append(row.ips * frac_int)
            transport_stats_bydate["coverage"].append(frac_day)
    # Discard nickname and sum reqs, ips, and coverage by date, fingerprint,
    # and transport.
    dir_stats_bydate = (
        pd.DataFrame(dir_stats_bydate)
            .drop("nickname", axis = 1)
            .groupby(["date", "fingerprint"], dropna = False)
            .sum()
            .reset_index()
    )
    transport_stats_bydate = (
        pd.DataFrame(transport_stats_bydate)
            .drop("nickname", axis = 1)
            .groupby(["date", "fingerprint", "transport"], dropna = False)
            .sum()
            .reset_index()
    )

    (
        # Join dir_stats and transport_stats by date, for each bridge instance.
        pd.merge(
            transport_stats_bydate.rename(columns = {"ips": "transport_ips", "coverage": "transport_coverage"}),
            dir_stats_bydate,
            on = ["date", "fingerprint"],
            how = "inner",
            suffixes = (None, None),
        )
        # "Bridges do not report directory requests by transport or IP version.
        # We approximate these numbers by multiplying the total number of
        # requests with the fraction of unique IP addresses by transport or IP
        # version."
        .assign(reqs = lambda df:
            df.groupby(["date", "fingerprint"], group_keys = False)
            .apply(lambda x: x["reqs"] * (x["transport_ips"] / x["transport_ips"].sum()))
        )
        .drop("transport_ips", axis = 1)
        # "First compute r(R) as the sum of reported successful directory
        # requests ... Estimate the number of clients per country and day using
        # the following formula:
        #   r(N) = floor(r(R) / frac / 10)"
        # We assume frac == 1.0, since the individual bridges we are looking at
        # always report directory request statistics.
        .assign(users = lambda x: x["reqs"] / 10)
        # Integrate the expected number of descriptors per day, based on the
        # history of tor instances per bridge.
        .assign(num_instances = lambda df:
            df.groupby(["fingerprint"], group_keys = False)
            .apply(lambda x: common.num_instances_on_dates(x["fingerprint"].iat[0], x["date"]))
        )
        # Compute the actual coverage for the day as the mean of the dir_stats
        # coverage and the transport_stats coverage. These may be slightly
        # different as one come from the dirreq-stats-end time series and the
        # other comes from the bridge-stats-end time series.
        .assign(coverage = lambda x: np.mean((x["coverage"], x["transport_coverage"]), axis = 0))
        .drop("transport_coverage", axis = 1)
        # Sort for a more readable CSV.
        .sort_values(["date", "fingerprint", "transport"])
        # Output to CSV.
        .to_csv(sys.stdout, index = False, float_format = "%.2f", columns = (
            "date",
            "fingerprint",
            "transport",
            "users",
            "num_instances",
            "coverage",
        ))
    )
