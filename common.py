import datetime

# Rounds a datetime down to midnight on the same date.
def datetime_floor(d):
    return d.replace(hour = 0, minute = 0, second = 0, microsecond = 0)

TIMEDELTA_1DAY = datetime.timedelta(seconds = 86400)
# Splits a datetime interval [begin, end] into subintervals, divided at
# midnight. Returns a generator over (sub_begin, frac_whole, frac_day) tuples,
# where sub_begin is the datetime beginning of the subinterval, frac_whole is
# the fraction between 0.0 and 1.0 that this subinterval represents of the whole
# [begin, end] interval, and frac_day is the fraction between 0.0 and 1.0 that
# this subinterval represents in the day in which it appears.
def segment_datetime_interval(begin, end):
    cur = begin
    while cur < end:
        next = min(datetime_floor(cur + TIMEDELTA_1DAY), end)
        delta = next - cur
        yield (cur.date(), delta / (end - begin), delta / TIMEDELTA_1DAY)
        cur = next

# These are tables of the dates on which bridge fingerprints changed their
# number of running tor instances with distinct nicknames. For background, see:
# https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Survival-Guides/Snowflake-Bridge-Installation-Guide
#
# The tables are intended to make it possible to compensate for days on which
# not all the bridge's tor instances published a descriptor, which would
# otherwise wrongly appear to be a temporary drop in usage, by dividing the
# number of descriptors per day ("coverage" in CSVs) by the number of instances
# per day. The count only represents the number of instances that were running
# *and actively used*. For example, we ran a practice load-balanced bridge from
# 2022-01-17 07:38:51 to 2022-02-03 05:39:24
# (https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40091#note_2770360)
# with 4 instances with nicknames flakey1–flakey4, in parallel to the
# then-production bridge with a single instance with nickname flakey. So you
# might expect that num_instances should be 5 starting on 2022-01-17 07:38:51,
# but we do not count that practice bridge because it was never actually
# distributed to users. Similarly, the switch to the bridge with 4 instances
# flakey1–flakey4 on 2022-01-25 17:41:00 increases the count to 4, not 5,
# because the single instance flakey stopped being used at that point.
#
# But the fact that a bridge instance is not used does not prevent it from
# publishing descriptors, and there are times when the number of actual
# descriptors published per day exceeds the nominal number expected here.
# Naively, this would cause us to *lower* the observed numbers for that day.
# But whenever we would divide by coverage by num_instances, we actually divide
# by max(coverage, num_instances), so the value do not end up being decreased.
# The rule of thumb is that it is okay to conservatively underestimate
# num_instances, because that results in a no-op: it is only if you
# overestimate num_instances that the value may be inflated above what they
# should be.
SNOWFLAKE_01_NUM_INSTANCES = (
    (datetime.datetime(2017,  1, 21,  0,  0,  0),  1), # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/18654#note_2591192
    (datetime.datetime(2022,  1, 25, 17, 41,  0),  4), # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40095#note_2772325
    (datetime.datetime(2022,  3, 16, 16, 51, 35),  8), # https://bugs.torproject.org/tpo/tpa/team/40664#note_2787624
    (datetime.datetime(2022,  4, 11, 14, 49, 30),  4), # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40111#note_2794860
    (datetime.datetime(2022,  9, 22, 20, 12, 32),  8), # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40173#note_2836743
    (datetime.datetime(2022,  9, 24, 21, 57, 56), 12), # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40173#note_2836743
)
SNOWFLAKE_02_NUM_INSTANCES = (
    # snowflake-02 started running with 4 instances. This fact is not
    # documented at # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40122,
    # but it's what the installation guide called for at the time:
    # https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Survival-Guides/Snowflake-Bridge-Installation-Guide?version_id=556ac519377293b52236897a01caf2b581dd01e2#tor.
    (datetime.datetime(2022,  4, 21,  0,  0,  0),  4), # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40122#note_2797271
    (datetime.datetime(2023,  2, 16, 22, 30, 57), 12), # https://bugs.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/40255#note_2879489
)

import bisect
def integrate_step_function(steps, x0, x1):
    i = bisect.bisect_right(steps, (x0,)) - 1
    y = steps[i][1]
    s = 0.0
    i += 1
    while i < len(steps) and steps[i] < (x1,):
        s += y * (steps[i][0] - x0).total_seconds()
        x0, y = steps[i]
        i += 1
    s += y * (x1 - x0).total_seconds()
    return s / 86400.0

# Returns a vector of numbers representing the average number of tor instances
# running on the 24 hours beginning at each date in dates, for the bridge with
# the given fingerprint. Returns a vector full of 1.0 if the bridge's history is
# not known.
def num_instances_on_dates(fingerprint, dates):
    try:
        steps = {
            "5481936581E23D2D178105D44DB6915AB06BFB7F": SNOWFLAKE_01_NUM_INSTANCES,
            "91DA221A149007D0FD9E5515F5786C3DD07E4BB0": SNOWFLAKE_02_NUM_INSTANCES,
        }[fingerprint]
    except KeyError:
        return dates.map(lambda _: 1.0)
    def map_one_date(date):
        x0 = datetime.datetime.combine(date, datetime.time(0))
        x1 = x0 + datetime.timedelta(days = 1)
        return integrate_step_function(steps, x0, x1)
    return dates.map(map_one_date)
